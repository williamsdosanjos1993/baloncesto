import Controlador.ActualizacionJugador;
import Controlador.CalculoVotos;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ModeloDatosTest {

    /*Inyectamos los datos mockeados con esta sentencia*/
    @InjectMocks
    private CalculoVotos calculoVotos;

    /*Creamos basicamente los datos que queremos mockear en el futuro con esta sentencia*/
    @Mock
    private ActualizacionJugador actualizacionJugador;

    /*Realizamos la prueba de la actualizacion, donde esperamos 5 votos de los datos mockeados
    * y esperamos 6 luego de hacer la prueba*/
    @Test
    public void actualizacionJugador(){

        when(actualizacionJugador.getVotos()).thenReturn(5);
        assertEquals(6,calculoVotos.sumaVotos());

    }
}
