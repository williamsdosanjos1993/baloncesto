import org.junit.Test;
import org.junit.Before;
import org.junit.After;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.core.IsNot.not;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Alert;
import org.openqa.selenium.Keys;
import java.util.*;
import java.net.MalformedURLException;
import java.net.URL;
public class PruebasPhantomjsIT {
    

    private WebDriver driver;
    private Map<String, Object> vars;
    JavascriptExecutor js;
    @Before
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", "./src/test/resources/chromedriver.exe");

        driver = new ChromeDriver();
        ChromeOptions options = new ChromeOptions();
        options.addArguments("headless");
        js = (JavascriptExecutor) driver;
        vars = new HashMap<String, Object>();
    }
    @After
    public void tearDown() {
        driver.quit();
    }

    /*Primera prueba funcional*/ 
    @Test
    public void pFA() {
        driver.get("http://localhost:8080/Baloncesto/index.html");
        driver.manage().window().setSize(new Dimension(968, 1040));
        driver.findElement(By.cssSelector("p:nth-child(11) > input")).click();
        /*Aqui se clicka el boton poner votos a 0*/ 
        driver.findElement(By.name("B3")).click();
        driver.findElement(By.linkText("Ir al comienzo")).click();
        driver.findElement(By.linkText("Ir Al Listado De Votos")).click();
        /*Aqui se compara que los valores sean 0 de los votantes*/ 
        assertThat(driver.findElement(By.cssSelector("tr:nth-child(1) > td:nth-child(3)")).getText(), is("0"));
        assertThat(driver.findElement(By.cssSelector("tr:nth-child(2) > td:nth-child(3)")).getText(), is("0"));
        assertThat(driver.findElement(By.cssSelector("tr:nth-child(3) > td:nth-child(3)")).getText(), is("0"));
        driver.findElement(By.linkText("Ir al comienzo")).click();
    }

   
    @Test
/*Segunda prueba funcional*/ 
    public void pFB() {
        driver.get("http://localhost:8080/Baloncesto/index.html");
        driver.manage().window().setSize(new Dimension(968, 1040));
        driver.findElement(By.name("txtOtros")).click();
        driver.findElement(By.name("txtOtros")).sendKeys("pruebaJugador");
        driver.findElement(By.cssSelector("p:nth-child(10)")).click();
        driver.findElement(By.cssSelector("p:nth-child(10) > input:nth-child(1)")).click();
        driver.findElement(By.name("B1")).click();
        driver.findElement(By.linkText("Ir al comienzo")).click();
        driver.findElement(By.linkText("Ir Al Listado De Votos")).click();
        assertThat(driver.findElement(By.cssSelector("tr:nth-child(4) > td:nth-child(2)")).getText(), is("pruebaJugador"));
        assertThat(driver.findElement(By.cssSelector("tr:nth-child(4) > td:nth-child(3)")).getText(), is("1"));
        driver.findElement(By.linkText("Ir al comienzo")).click();
    }

   
}
