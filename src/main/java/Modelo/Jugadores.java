package Modelo;

public class Jugadores {

    private int id;
    private String jugador;
    private int votos;

    public Jugadores() {
    }

    public Jugadores(int id, String jugador, int votos) {
        this.id = id;
        this.jugador = jugador;
        this.votos = votos;
    }

   

    public int getId() {
        return id;
    }


    public void setId(int id) {
        this.id = id;
    }

    public String getJugador() {
        return jugador;
    }

    public void setJugador(String jugador) {
        this.jugador = jugador;
    }

    public int getVotos() {
        return votos;
    }

    public void setVotos(int votos) {
        this.votos = votos;
    }


}