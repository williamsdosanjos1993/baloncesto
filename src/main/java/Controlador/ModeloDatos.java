package Controlador;

import Modelo.Jugadores;

import java.sql.*;
import java.util.ArrayList;

import java.util.logging.Logger;


public class ModeloDatos {

    private Connection con;
    private Statement set;
    private ResultSet rs;

    private static final String error="El error es: ";


    Logger logger = Logger.getLogger("MyLog");

    public void abrirConexion() {

        try {
            Class.forName("com.mysql.cj.jdbc.Driver");

            // Con variables de entorno
            String dbHost = System.getenv().get("DATABASE_HOST");
            String dbPort = System.getenv().get("DATABASE_PORT");
            String dbName = System.getenv().get("DATABASE_NAME");
            String dbUser = System.getenv().get("DATABASE_USER");
            String dbPass = System.getenv().get("DATABASE_PASS");

            String url = dbHost + ":" + dbPort + "/" + dbName;
            con = DriverManager.getConnection(url, dbUser, dbPass);

        } catch (Exception e) {
            // No se ha conectado
            logger.info("No se ha podido conectar");
            logger.info(error + e.getMessage());

        }
    }

    public boolean existeJugador(String nombre) {
        boolean existe = false;
        String cad;
        try {
            set = con.createStatement();
            rs = set.executeQuery("SELECT * FROM Jugadores");
            while (rs.next()) {
                cad = rs.getString("Nombre");
                cad = cad.trim();
                if (cad.compareTo(nombre.trim()) == 0) {
                    existe = true;
                }
            }
            rs.close();
            set.close();
        } catch (Exception e) {
            // No lee de la tabla
            logger.info("No lee de la tabla");
            logger.info(error + e.getMessage());
        }
        return (existe);
    }



    public void actualizarJugador(String nombre) {
        try {
            set = con.createStatement();
            set.executeUpdate("UPDATE Jugadores SET votos=votos+1 WHERE nombre " + " LIKE '%" + nombre + "%'");
            rs.close();
            set.close();
        } catch (Exception e) {
            // No modifica la tabla
            logger.info("No modifica la tabla");
            logger.info(error + e.getMessage());
        }
    }

    public void insertarJugador(String nombre) {
        try {
            set = con.createStatement();
            set.executeUpdate("INSERT INTO Jugadores " + " (nombre,votos) VALUES ('" + nombre + "',1)");
            rs.close();
            set.close();
        } catch (Exception e) {
            // No inserta en la tabla
            logger.info("No inserta en la tabla");
            logger.info(error + e.getMessage());
        }
    }


   

    public ArrayList <Jugadores> consultaBD() {
        
        ArrayList <Jugadores> listaJugadores = null;

        try {
            abrirConexion();
            set = con.createStatement();
            rs = set.executeQuery("SELECT * FROM Jugadores");
            
            listaJugadores = new ArrayList<Jugadores>();
            while (rs.next()) {
                Jugadores j =new Jugadores();
                j.setId(rs.getInt("id"));
                j.setJugador(rs.getString("nombre"));
                j.setVotos(rs.getInt("votos")) ;
                listaJugadores.add(j);
                
            }
           
            set.close();
        } catch (Exception e) {
            // No inserta en la tabla
            logger.info("No se pudo hacer la consulta");
            logger.info(error + e.getMessage());
        }
        return listaJugadores;
   
    }

    public void borrarVotos() {
        try {
            set = con.createStatement();
            set.executeUpdate("Update  Jugadores set votos=0");
            rs.close();
            set.close();
        } catch (Exception e) {
            // No inserta en la tabla
            logger.info("No se pudo borrar los votos");
            logger.info(error + e.getMessage());
        }
    }


    public void cerrarConexion() {
        try {
            con.close();
        } catch (Exception e) {
            logger.info(e.getMessage());
        }
    }

}
