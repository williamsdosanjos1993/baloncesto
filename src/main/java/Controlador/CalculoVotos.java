package Controlador;

import Controlador.ActualizacionJugador;

public class CalculoVotos implements SumaVotos {

    private ActualizacionJugador actualizacionJugador;
    @Override
    public int sumaVotos() {
        return actualizacionJugador.getVotos()+1;
    }
}
