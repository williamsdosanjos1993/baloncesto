<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="Controlador.ModeloDatos"%>
<%@ page import="Modelo.Jugadores"%>
<%@ page import="java.util.ArrayList"%>

            <!DOCTYPE html>

<html lang="es">

            <head>
                <title>Ver jugadores</title>
            </head>

            <body>

                <div>
                    <table>
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Jugador</th>
                                <th>Votos</th>
                            </tr>


                        </thead>
                        <tbody>
                            <%
                           ModeloDatos d = new ModeloDatos();
                            ArrayList<Jugadores> listaJugadores = d.consultaBD();
                                for(Jugadores j : listaJugadores){
                                out.println("<tr>");
                                    out.println("<td>"+j.getId()+"</td>");
                                    out.println("<td>"+j.getJugador()+"</td>");
                                    out.println("<td>"+j.getVotos()+"</td>");
                                    out.println("</tr>");
                                }
                                %>
                        </tbody>
                    </table>
                </div>


                <br> <a href="index.html"> Ir al comienzo</a>
            </body>

            </html>
